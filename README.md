# Description
A GUI for use with GLS API. It loads a file containing names and addresses, does some checking to ensure the formatting is correct per GLS API requirement and then for each name entry creates a PDF with the corresponding label.

# Requirements
Requires an agreement with GLS, specifically a username and password. GLS provides the following for testing purposes:

username: 2080060960

password: API1234

The project is coded in Python3 with the PyQt5 library.

# To-does
- [x] Use Thread Pool instead of a single Thread for faster label creation
- [ ] Use a File-dialog to choose an output folder for the labels
- [ ] Localization—Danish and British English
- [ ] Include proper settings
  - [ ] Default value for weight
  - [ ] Behaviour of weight—overwrite or not
  - [ ] Duplicate rows—remove or not
  - [ ] Change language
- [ ] Masking of password
- [ ] Better input checking, such as password and weight
- [ ] Progress bar or similar for the thread doing label creation
- [ ] Thread the analyser as this may also be slow if enough checks are added, and the file is big enough
- [ ] Clean up of temporary files and/or circumvent the usage of temporary files