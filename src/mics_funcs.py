import csv


# Converts a zip code to city name using list from PostNord
def zip_to_city(zip_code):
    csv_zip = csv.reader(open('../data/postnumre.csv', 'r'), delimiter=',')
    next(csv_zip)
    for line in csv_zip:
        if zip_code == line[0]:
            return line[1]


# Checks if a string is either empty or consists of spaces
def empty_check(field: str):
    if not field:
        return True
    elif field.isspace():
        return True
    else:
        return False


def common_element(list1, list2):
    return list(set(list1) & set(list2))


def common_indices(list1, list2):
    return [list1.index(x) for x in list2 if x in list1]
