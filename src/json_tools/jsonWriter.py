import csv
import requests
import base64
import os
from mics_funcs import zip_to_city
from pathlib import Path

# User input variables
country = 'Danmark'
address_list = 'adresses'


def json_writer(info, username='2080060960', password='API1234', weight=1):
    city_name = zip_to_city(info['postnr'])

    json_dict = "{\r\n" \
                f"\"UserName\": \"{username}\",\r\n" \
                f"\"Password\": \"{password}\",\r\n" \
                "\"Customerid\": \"2080060960\",\r\n" \
                "\"Contactid\": \"208a144Uoo\",\r\n" \
                "\"ShipmentDate\": \"20220222\",\r\n" \
                "\"Reference\": \"Customers reference\",\r\n" \
                "\"Addresses\": {\r\n" \
                "\"Delivery\": {\r\n" \
                f"\"Name1\": \"{info['navn']}\",\r\n" \
                f"\"Street1\": \"{info['adresse']}\",\r\n" \
                "\"CountryNum\": \"208\",\r\n" \
                f"\"ZipCode\": \"{info['postnr']}\",\r\n" \
                f"\"City\": \"{city_name}\"," "\r\n  " \
                f"\"Contact\": \"{info['navn']}\",\r\n" \
                f"\"Email\": \"{info['email']}\",\r\n " \
                "\"Phone\": \"\",\r\n " \
                f"\"Mobile\": \"{info['mobil']}\"\r\n" \
                "},\r\n" \
                "\r\n },\r\n" \
                "\"Parcels\": [\r\n " " {\r\n" \
                f"\"Weight\": {weight},\r\n" \
                "}\r\n    ]," "\r\n " \
                "\"Services\": {\r\n" \
                "\"ShopDelivery\": \"96600\",\r\n }\r\n} "

    return json_dict


def create_label(info, username='2080060960', password='API1234', url='http://api.gls.dk/ws/DK/V1/CreateShipment', weight=1, id='1'):
    json_dict = json_writer(info, username, password, weight)
    headers = {
        'Content-Type': 'application/json; charset=utf-8'
    }
    payload = json_dict.encode('utf-8')
    response = requests.request("POST", url, headers=headers, data=payload)
    # print(payload)
    # print(response.text)
    print(response)
    base64_pdf_bytes = response.json()['PDF'].encode('utf-8')
    pdf_name = info['navn'] + '_' + id
    pdf_path = f'{pdf_name}.pdf'
    pdf_path = str(Path.home()) + '\\pakke-labels\\' + pdf_path
    os.makedirs(os.path.dirname(pdf_path), exist_ok=True)

    with open(pdf_path, 'wb') as file_to_save:
        decoded_image_data = base64.decodebytes(base64_pdf_bytes)
        file_to_save.write(decoded_image_data)
