from PyQt5.QtWidgets import (QMainWindow, QTextEdit, QMessageBox, QGridLayout, QLabel, QPushButton,
                             QFileDialog, QApplication, QLineEdit, QWidget, QComboBox, QProgressBar)
from PyQt5.QtGui import (QIcon)
from PyQt5.QtCore import *
import traceback
import sys
import os
import csv
from pathlib import Path
from csv_tools import csvSanitizer
from csv_tools import csvAnalyser
from csv_tools import csvHarmonizer
from json_tools import jsonWriter


def print_output(s):
    print(s)


def thread_complete():

    print("THREAD COMPLETE!")
    temp_file = '../data/temp.csv'
    if os.path.isfile(temp_file):
        os.remove(temp_file)


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):

        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class Example(QMainWindow):
    def __init__(self):
        super().__init__()

        self.counter = 0
        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
        # self.timer = QTimer()
        # self.timer.setInterval(1000)
        # self.timer.timeout.connect(self.recurring_timer)
        # self.timer.start()

        # Open file button
        self.open_file = QPushButton('Open')
        self.open_file.setShortcut('Ctrl+O')
        self.open_file.setStatusTip('Open new file')
        self.open_file.clicked.connect(self.open_file_action)

        self.sanitize_file = QPushButton('Sanitize')
        self.sanitize_file.setShortcut('Ctrl+S')
        self.sanitize_file.setStatusTip('Sanitize')
        self.sanitize_file.clicked.connect(self.sanitize_file_action)
        # Disable sanitize from the start
        self.sanitize_file.setEnabled(False)

        self.harmonize_file = QPushButton('Harmonize')
        self.harmonize_file.setShortcut('Ctrl+H')
        self.harmonize_file.setStatusTip('Harmonize')
        self.harmonize_file.clicked.connect(self.harmonize_file_action)
        self.harmonize_file.setEnabled(False)

        self.analyse_file = QPushButton('Analyse')
        self.analyse_file.setShortcut('Ctrl+A')
        self.analyse_file.setStatusTip('Analyse')
        self.analyse_file.clicked.connect(self.analyse_file_action)
        self.analyse_file.setEnabled(False)

        self.create_label = QPushButton('Create label')
        self.create_label.setShortcut('Ctrl+C')
        self.create_label.setStatusTip('Create label')
        self.create_label.clicked.connect(self.create_label_action)
        self.create_label.setEnabled(False)

        # User and password
        self.userEdit = QLineEdit('2080060960')
        self.user = QLabel('Brugernavn')
        self.passwordEdit = QLineEdit('API1234')
        # self.passwordEdit.setEchoMode(QLineEdit.Password)
        self.password = QLabel('Kodeord')

        # Weight and package-type
        self.weightEdit = QLineEdit()
        # self.weightEdit.setValidator(QDoubleValidator(0.0, 9, 1, notation=QDoubleValidator.StandardNotation))
        self.weight = QLabel('Vægt')
        self.packageEdit = QComboBox()
        self.packageEdit.addItems(["ShopDeliveryService", "BusinessParcel"])
        self.package = QLabel('Pakke-type')

        self.errorEdit = QTextEdit()
        self.errorEdit.setReadOnly(True)
        self.error = QLabel('Errors:')

        self.textEdit = QTextEdit()
        self.textEdit.setReadOnly(True)
        self.text = QLabel('Output:')

        self.progressBar = QProgressBar(self)
        self.progressBar.setAlignment(Qt.AlignCenter)

        # Set up widget with buttons
        layout = QGridLayout()
        widget = QWidget()
        layout.addWidget(self.open_file, 1, 0)
        layout.addWidget(self.sanitize_file, 1, 1)
        layout.addWidget(self.harmonize_file, 1, 2)
        layout.addWidget(self.analyse_file, 1, 3)
        layout.addWidget(self.create_label, 1, 4)

        layout.addWidget(self.user, 2, 0)
        layout.addWidget(self.userEdit, 2, 1)
        layout.addWidget(self.password, 2, 3)
        layout.addWidget(self.passwordEdit, 2, 4)

        layout.addWidget(self.weight, 3, 0)
        layout.addWidget(self.weightEdit, 3, 1)
        layout.addWidget(self.package, 3, 3)
        layout.addWidget(self.packageEdit, 3, 4)

        layout.addWidget(self.error, 4, 0)
        layout.addWidget(self.errorEdit, 4, 1, 1, 5)

        layout.addWidget(self.text, 5, 0)
        layout.addWidget(self.textEdit, 5, 1, 5, 5)

        layout.addWidget(self.progressBar, 10, 1, 5, 5)

        widget.setLayout(layout)
        self.setCentralWidget(widget)

        self.setGeometry(300, 300, 900, 500)
        self.setWindowTitle('GLS Label Creator')
        self.setWindowIcon(QIcon('../icns/doc_export_icon&48.png'))
        self.statusBar().showMessage('Ready')
        self.show()

    def open_file_action(self):

        home_dir = str(Path.home())
        fname = QFileDialog.getOpenFileName(self, 'Open file', home_dir)

        # Disable all buttons if new file in case they are 'on'
        self.sanitize_file.setEnabled(False)
        self.harmonize_file.setEnabled(False)
        self.analyse_file.setEnabled(False)
        self.create_label.setEnabled(False)

        # Clear error-log
        self.errorEdit.setText('')

        if fname[0]:
            f = open(fname[0], 'r')

            data = f.read()
            line_count = sum(1 for line in open(fname[0], 'r'))
            self.textEdit.setText(data)
            self.statusBar().showMessage(f'{line_count} number of lines loaded')
            # Enable next step, i.e. sanitation
            self.sanitize_file.setEnabled(True)

    def sanitize_file_action(self):
        editor_text = self.textEdit.toPlainText()
        if not csvHarmonizer.empty_check(str(editor_text)):
            temp_file = '../data/temp.csv'
            with open(temp_file, 'w') as tmp:
                tmp.write(editor_text)

            sanitized_file = '../data/sanitized.csv'

            csvSanitizer.sanitizer('../data/temp.csv')
            with open(sanitized_file, 'r') as f:
                data = f.read()
                self.textEdit.setText(data)

            if os.path.isfile(sanitized_file):
                os.remove(sanitized_file)
                os.remove(temp_file)
                # Enable next step, i.e. harmonization
                self.harmonize_file.setEnabled(True)

        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText('You need to load a non-empty file!')
            msg.setWindowTitle("Error")
            msg.exec_()

    def harmonize_file_action(self):
        editor_text = self.textEdit.toPlainText()
        temp_file = '../data/temp.csv'
        with open(temp_file, 'w') as tmp:
            tmp.write(editor_text)

        harmonized_file = '../data/harmonized.csv'

        csvHarmonizer.harmonizer('../data/temp.csv')
        with open(harmonized_file, 'r') as f:
            data = f.read()
            self.textEdit.setText(data)

        if os.path.isfile(harmonized_file):
            os.remove(harmonized_file)
            os.remove(temp_file)
            # Enable next step, i.e. analysis
            self.analyse_file.setEnabled(True)

    def analyse_file_action(self):
        editor_text = self.textEdit.toPlainText()
        temp_file = '../data/temp.csv'
        with open(temp_file, 'w') as tmp:
            tmp.write(editor_text)

        errors_file = '../data/analysis_errors.txt'

        if os.path.isfile(errors_file):
            os.remove(errors_file)

        csvAnalyser.analyser('../data/temp.csv')

        self.textEdit.setText(open('../data/analysis_temp.txt', 'r').read())

        with open(errors_file, 'r') as f:
            data = f.read()
            self.errorEdit.setText(data)
            # If the error file is empty or white space, then no error occurred
            with open('../data/analysis_errors.txt', 'rb') as f:
                f.seek(-2, os.SEEK_END)
                while f.read(1) != b'\n':
                    f.seek(-2, os.SEEK_CUR)
                last_line = f.readline().decode()
            if last_line == 'Error code: ':
                self.create_label.setEnabled(True)

        if os.path.isfile(errors_file):
            os.remove(errors_file)
            os.remove(temp_file)
            os.remove('../data/analysis_temp.txt')

    def progress_fn(self, n):
        print("%d%% done" % n)

    def create_label_function(self, username, password, weight, progress_callback):
        editor_text = self.textEdit.toPlainText()
        temp_file = '../data/temp.csv'
        with open(temp_file, 'w') as tmp:
            tmp.write(editor_text)

        with open(temp_file, 'r') as tmp:
            row_sum = sum(1 for row in tmp)
            row_sum -= 1

        csv_reader = csv.reader(open(temp_file, 'r'), delimiter=',')
        csv_headings = next(csv_reader)

        line_count = 1

        for row in csv_reader:
            info_dict = dict(zip(csv_headings, row))
            unique_id = f'{line_count}'
            jsonWriter.create_label(info=info_dict, username=username, password=password, weight=weight, id=unique_id)
            # progress_callback.emit(round(line_count * 100 / row_sum))
            current_percentage = round(line_count * 100 / row_sum)
            QMetaObject.invokeMethod(self.progressBar, "setValue", Qt.QueuedConnection, Q_ARG(int, current_percentage))
            print(round(line_count * 100 / row_sum))
            # self.progressBar.setValue(round(line_count * 100 / row_sum))
            line_count += 1

    def create_label_action(self):

        user_text = self.userEdit.text()
        password_text = self.passwordEdit.text()
        weight_text = self.weightEdit.text()

        if not csvHarmonizer.empty_check(user_text) and not csvHarmonizer.empty_check(password_text) and not csvHarmonizer.empty_check(weight_text):
            # worker = Worker()
            # self.worker.moveToThread(self.thread)
            # self.thread.started.connect(partial(self.worker.run, user_text, password_text, weight_text))
            # self.worker.finished.connect(self.thread.quit)
            # self.worker.finished.connect(self.worker.deleteLater)
            # self.thread.finished.connect(self.thread.deleteLater)
            # self.worker.progress.connect(self.report_progress)
            # self.thread.start()
            worker = Worker(self.create_label_function, user_text, password_text, weight_text)
            # worker.signals.result.connect(print_output)
            worker.signals.finished.connect(thread_complete)
            # worker.signals.progress.connect(self.progress_fn)

            # Execute
            self.threadpool.start(worker)

        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText('You need to specify Brugernavn, Kode og Vægt!')
            msg.setWindowTitle("Error")
            msg.exec_()

        # Final resets
        self.sanitize_file.setEnabled(False)
        self.harmonize_file.setEnabled(False)
        self.analyse_file.setEnabled(False)
        self.create_label.setEnabled(False)
        # self.thread.finished.connect()

    def report_progress(self, n):
        self.error.setText(f"Filnummer {n}")


def main():
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
