import csv
import os
from mics_funcs import common_element, empty_check, common_indices
import pandas

necessaryName = ['navn', 'name']
streetName = ['vej', 'vejnr', 'vejnummer', 'street', 'streetname']
addressName = ['adresse', 'address']
necessaryAddress = streetName + addressName
necessaryZip = ['postnummer', 'postnr', 'zip', 'zipcode']
phoneName = ['telefon', 'telefonnummer', 'telefonnr', 'mobil', 'mobilnummer', 'mobilnr']
emailName = ['email', 'e-mail', 'mail']
necessaryContactInfo = phoneName + emailName


def analyser(filename):
    accept_email = 'Email is available\n'
    accept_phone = 'Phone is available\n'
    csv_file = csv.reader(open(filename, 'r'), delimiter=',')
    csv_copy = pandas.read_csv(filename, keep_default_na=False)
    error_path = '../data/analysis_errors.txt'
    if os.path.isfile(error_path):
        os.remove(error_path)
    error_file = open(error_path, 'a+')
    headings = next(csv_file)
    error_code = ''

    if not common_element(necessaryName, headings):
        error_file.write('Name column needs to be given!\n')
        error_code = 'name'

    if not common_element(necessaryAddress, headings):
        error_file.write('Address column needs to be given!\n')
        error_code = error_code + 'address'

    if not common_element(necessaryZip, headings):
        error_file.write('Zip code column needs to be given!\n')
        error_code = error_code + 'zip'

    if not common_element(necessaryContactInfo, headings):
        error_file.write('Either email or phone column needs to be given!\n')
        error_code = error_code + 'contact_info'
        contact_info = common_element(necessaryContactInfo, headings)
    elif common_element(phoneName, headings) and not common_element(emailName, headings):
        error_file.write(accept_phone)

    elif common_element(emailName, headings) and not common_element(phoneName, headings):
        error_file.write(accept_email)

    else:
        error_file.write(accept_phone)
        error_file.write(accept_email)
    error_file.close()
    use_phone = False
    use_email = False
    with open(error_path) as f:
        fo = f.read()
        if accept_phone in fo:
            use_phone = True
        if accept_email in fo:
            use_email = True

    error_file = open(error_path, 'a+', newline='')

    if error_code == '':
        phone = common_element(headings, phoneName)[0]
        email = common_element(headings, emailName)[0]

        index_name = common_indices(headings, necessaryName)[0]
        index_street = common_indices(headings, necessaryAddress)[0]
        index_zip = common_indices(headings, necessaryZip)[0]

        line_count = 1
        for row in csv_file:
            index = line_count - 1
            line_count += 1
            if len(row[index_name]) > 40 or empty_check(row[index_name]):
                error_file.write(f'Line {line_count}: name is too long or missing\n')

            if len(row[index_street]) > 40 or empty_check(row[index_street]):
                error_file.write(f'Line {line_count}: address is too long or missing\n')

            if len(row[index_zip]) > 10 or empty_check(row[index_zip]):
                error_file.write(f'Line {line_count}: zip code is too long or missing\n')

            if use_phone and not use_email:
                index_phone = common_indices(headings, phoneName)[0]
                if len(row[index_phone]) > 40 or empty_check(row[index_phone]):
                    error_file.write(f'Line {line_count}: phone number is missing or too long\n')
            elif not use_phone and use_email:
                index_email = common_indices(headings, emailName)[0]
                if len(row[index_email]) > 100 or '@' not in row[index_email]:
                    error_file.write(f'Line {line_count}: email is not correct format\n')
            elif use_phone and use_email:
                index_phone = common_indices(headings, phoneName)[0]
                index_email = common_indices(headings, emailName)[0]
                if len(row[index_phone]) > 40:
                    csv_copy.loc[index, phone] = ''
                if len(row[index_email]) > 100 or '@' not in row[index_email]:
                    csv_copy.loc[index, email] = ''
                if not csv_copy.loc[index, phone] and not csv_copy.loc[index, email]:
                    error_file.write(f'Line {line_count}: Neither phone nor email is correct format\n')
                    error_code = error_code + 'contact_info'

    csv_copy.to_csv('../data/analysis_temp.txt', index=False)
    error_file.write(f'Error code: {error_code}')
