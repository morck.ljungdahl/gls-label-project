import csv


# Should probably also sanitize the rows instead of only the headers, such as
# 1. making empty entries truly empty
# 2. remove double spaces in fields
# 3. remove additional spaces characters beside dots
# 4. add option for removing duplicate entries
def sanitizer(filename):
    csv_file = csv.reader(open(filename, 'r'), delimiter=',')
    csv_sanitized = csv.writer(open('../data/sanitized.csv', 'w', newline=''))
    csv_headings = next(csv_file)
    column_names = [string.lower() for string in csv_headings]
    column_names = [string.replace(' ', '') for string in column_names]
    column_names = [string.replace('.', '') for string in column_names]
    column_names = [string.replace('nummer', 'nr') for string in column_names]
    csv_sanitized.writerow(column_names)
    csv_sanitized.writerows(csv_file)
