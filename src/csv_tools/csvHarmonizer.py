import csv
from mics_funcs import empty_check


# Removes arbitrary set of indices from a set
def remove_index_set(a, indices):
    setCopy = a[:]  # otherwise set might be changed
    for index in sorted(indices, reverse = True):
        del setCopy[index]
    return setCopy


def harmonizer(filename):
    csv_file = csv.reader(open(filename, 'r'), delimiter=',')
    csv_harmonized = csv.writer(open('../data/harmonized.csv', 'w', newline=''))
    headings = next(csv_file)
    if 'mellemnavn' in headings:
        index_for = headings.index('fornavn')
        index_mellem = headings.index('mellemnavn')
        index_efter = headings.index('efternavn')
        indices = [index_for, index_mellem, index_efter]
        non_name_headings = remove_index_set(headings, indices)
        csv_harmonized.writerow(['navn', *non_name_headings])

        for rowSanitized in csv_file:
            rowSanitizedNonName = remove_index_set(rowSanitized, indices)
            if not empty_check(rowSanitized[index_mellem]):
                new_row = [' '.join([rowSanitized[index_for], rowSanitized[index_mellem], rowSanitized[index_efter]]),
                           *rowSanitizedNonName]
                csv_harmonized.writerow(new_row)
            else:
                new_row = [' '.join([rowSanitized[index_for], rowSanitized[index_efter]]), *rowSanitizedNonName]
                csv_harmonized.writerow(new_row)

    else:
        csv_harmonized.writerow(headings)
        csv_harmonized.writerows(csv_file)